from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup


my_url = 'https://www.newegg.com/global/nz/Desktop-Graphics-Cards/SubCategory/ID-48'

#Opening up connection, grabbing the page
uClient = uReq(my_url)

#Puts content in variatble
page_html = uClient.read()

#Closes the client
uClient.close()

#Call BeautifulSoup, html parsing
page_soup = soup(page_html, "html.parser")

#Grabs each product
containers = page_soup.findAll("div",{"class":"item-container"})

filename = "products2.csv"
f = open(filename, "w")

headers = "brand, product_name, shipping\n"

f.write(headers)

for container in containers:
    brand = container.div.div.a.img["title"]

    title_container = container.findAll("a", {"class":"item-title"})
    product_name = title_container[0].text

    shipping_container = container.findAll("li", {"class":"price-ship"})
    shipping = shipping_container[0].text.strip()

    print("Brand: " + brand)
    print("Product_name: " + product_name)
    print("Shipping: " + shipping)

    f.write(brand + "," + product_name.replace(",", "|") + "," + shipping + "\n")

f.close()
