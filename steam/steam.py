from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup
from tkinter import*
import csv

class displayData:
    def display(self):

        sample = open('sales.csv', 'r', encoding="utf8")

        csv1 = csv.DictReader(sample)

        rownum = 0
        current = []

        for row in csv1:
            current.append (row)
            rownum += 1

        currentLen = len(current)
        count = 0
        
        saleFrame1 = Frame(saleFrame)
        saleFrame1.pack(side=LEFT)
        title = Label(saleFrame1, text="Product name")
        title.pack()
        while count < 26:
            col = current[count]
            funct = col['title']
            emoji_pattern = re.compile("["
                u"\U0001F600-\U0001F64F"  # emoticons
                u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                u"\U0001F680-\U0001F6FF"  # transport & map symbols
                u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                               "]+", flags=re.UNICODE)
            tex = Label(saleFrame1, text=emoji_pattern.sub(r'', funct))
            tex.pack()
            can = Canvas(saleFrame1, width=400, height=5)
            can.pack()
            can.create_line(0, 2.5, 400, 2.5)
            count = count + 1

        count = 0
        saleFrame2 = Frame(saleFrame)
        saleFrame2.pack(side=LEFT)
        title = Label(saleFrame2, text="Original Price")
        title.pack()
        while count < 26:
            col = current[count]
            funct = col['price_original']
            tex = Label(saleFrame2, text=funct)
            tex.pack()
            can = Canvas(saleFrame2, width=100, height=5)
            can.pack()
            can.create_line(0, 2.5, 100, 2.5)
            count = count + 1

        count = 0
        saleFrame2 = Frame(saleFrame)
        saleFrame2.pack(side=LEFT)
        title = Label(saleFrame2, text="Discounted Price")
        title.pack()
        while count < 26:
            col = current[count]
            funct = col['price_discount']
            tex = Label(saleFrame2, text=funct)
            tex.pack()
            can = Canvas(saleFrame2, width=100, height=5)
            can.pack()
            can.create_line(0, 2.5, 100, 2.5)
            count = count + 1

        count = 0
        saleFrame2 = Frame(saleFrame)
        saleFrame2.pack(side=LEFT)
        title = Label(saleFrame2, text="Discount")
        title.pack()
        while count < 26:
            col = current[count]
            funct = col['discount ']
            tex = Label(saleFrame2, text=funct)
            tex.pack()
            can = Canvas(saleFrame2, width=100, height=5)
            can.pack()
            can.create_line(0, 2.5, 100, 2.5)
            count = count + 1


root = Tk()

menu = Menu(root)
root.config(menu=menu)

#*** Main Menu - add commands later***

subMenu = Menu(menu)
menu.add_cascade(label="File", menu=subMenu)
subMenu.add_command(label="New Project...")
subMenu.add_command(label="New...")
subMenu.add_separator()
subMenu.add_command(label="Exit")

editMenu = Menu(menu)
menu.add_cascade(label="Edit", menu=editMenu)
editMenu.add_command(label="Redo")

# *** Toolbar - add commands later***

toolbar = Frame(root, bg="blue")

latestButt = Button(toolbar, text="Latest Steam Sales")
latestButt.pack(side=LEFT, padx=2, pady=2)
newButt = Button(toolbar, text="Pull New Steam Sales")
newButt.pack(side=LEFT, padx=2, pady=2)

toolbar.pack(side=TOP, fill=X)

saleFrame = Frame(root, height=1000, width=1000)
saleFrame.pack()

g = displayData()
g.display()

root.mainloop()


