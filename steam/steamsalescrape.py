from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup
import csv
import numpy as np

#Opening connecting, grabbing steam sale page 1. Closing connection. Prasing page
my_url = 'https://store.steampowered.com/search/?specials=1&os=win'
uClient =uReq(my_url)
page_html =uClient.read()
uClient.close()
page_soup = soup(page_html, "html.parser")

#Number of Sales Pages

salesContainers = page_soup.find("div",{"class":"search_pagination_left"})
salesPages = salesContainers.text.strip()
qtyPages = salesPages.split(' ')[3:4]
qtyPages = (np.int_(qtyPages))
totalItems = salesPages.split(' ')[5:6]
totalItems = (np.int_(totalItems))
numPages = (totalItems // qtyPages) + 1
print(numPages)

#Make CSV file
filename = "sales.csv"
f = open(filename, "w", encoding='utf8')

#Locating links to products on sale
containers = page_soup.findAll("div",{"class":"responsive_search_name_combined"})

headers = "title,price_original,price_discount,discount \n"

f.write(headers)

for container in containers:

    name_container = container.findAll("span", {"class":"title"})
    title = name_container[0].text.strip()

    originalPrice_container = container.findAll("div", {"class":"col search_price discounted responsive_secondrow"})
    price_original = originalPrice_container[0].text.strip()
    price_original = 'NZ'.join(price_original.split('NZ')[:2])
    
    originalPrice_container = container.findAll("div", {"class":"col search_price discounted responsive_secondrow"})
    price_discount = originalPrice_container[0].text.strip()
    price_discount = 'NZ'.join(price_discount.split('NZ')[2:3])

    discount_container = container.findAll("div", {"class":"col search_discount responsive_secondrow"})
    discount = discount_container[0].text.strip()
    
    f.write(title + ","+ price_original + "," + " NZ" + price_discount + "," + discount + "\n")
    
f.close()

print("Finished")

