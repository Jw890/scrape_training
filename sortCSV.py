import csv
import operator

print('From CSV')

sample = open('sampleData.txt','r')

csv1 = csv.reader(sample,delimiter=',')

sort = sorted(csv1,key=operator.itemgetter(1))

for eachline in sort:
    print(eachline)

print('from list')

sample2 = [(5,6),(7,1),(3,2)]

sort = sorted(sample2,key=operator.itemgetter(0))

for eachline in sort:
    print(eachline)

#Put CSV in a list

sample = open('sampleData.txt','r')

csv1 = csv.reader(sample,delimiter=',')

rownum = 0	

a = []

for row in csv1:
    a.append (row)
    rownum += 1

