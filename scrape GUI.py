from urllib.request import urlopen as uReq
from bs4 import BeautifulSoup as soup
from tkinter import*
import csv
import collections


        #self.scrapeButton = Button(frame, text="Scrape Steam Sales", command=Scrape.scrapeFunction)
        #self.scrapeButton.pack()

class displayData:
    def display(self):
        
        print('active')
        sample = open('products2.csv','r')

        csv1 = csv.DictReader(sample)

        count = 0
        rownum = 0

        current = []

        for row in csv1:
            current.append (row)
            rownum += 1

        currentLen = len(current)

        saleFrame1 = Frame(saleFrame)
        saleFrame1.pack(side=LEFT)
        while count < currentLen:
            col = current[count]
            brand = col['brand']
            tex = Label(saleFrame1, text=brand)
            tex.pack()
            can = Canvas(saleFrame1, width=100, height=5)
            can.pack()
            can.create_line(0, 2.5, 100, 2.5)
            count = count + 1

        count = 0
        saleFrame2 = Frame(saleFrame)
        saleFrame2.pack(side=LEFT)
        
        while count < currentLen:
            col = current[count]
            brand = col[' product_name']
            tex = Label(saleFrame2, text=brand)
            tex.pack()
            can = Canvas(saleFrame2, width=1200, height=5)
            can.pack()
            can.create_line(0, 2.5, 1200, 2.5)
            count = count + 1
        
        #lb1 = Listbox(saleFrame)
        #while count < currentLen:
            #col = current[count]
            #lb1.insert(count, col['brand'])
            #print(col['brand'])
            #count = count + 1

        #lb1.pack()

class Scrape:
    def scrapeFunction():
        my_url = 'https://www.newegg.com/global/nz/Desktop-Graphics-Cards/SubCategory/ID-48'

        #Opening up connection, grabbing the page
        uClient = uReq(my_url)

        #Puts content in variatble
        page_html = uClient.read()

        #Closes the client
        uClient.close()

        #Call BeautifulSoup, html parsing
        page_soup = soup(page_html, "html.parser")

        #Grabs each product
        containers = page_soup.findAll("div",{"class":"item-container"})

        filename = "products2.csv"
        f = open(filename, "w")

        headers = "brand, product_name, shipping\n"

        f.write(headers)

        for container in containers:
            brand = container.div.div.a.img["title"]

            title_container = container.findAll("a", {"class":"item-title"})
            product_name = title_container[0].text

            shipping_container = container.findAll("li", {"class":"price-ship"})
            shipping = shipping_container[0].text.strip()

            print("Brand: " + brand)
            print("Product_name: " + product_name)
            print("Shipping: " + shipping)

            f.write(brand + "," + product_name.replace(",", "|") + "," + shipping + "\n")

        f.close()

root = Tk()

menu = Menu(root)
root.config(menu=menu)

#*** Main Menu - add commands later***

subMenu = Menu(menu)
menu.add_cascade(label="File", menu=subMenu)
subMenu.add_command(label="New Project...")
subMenu.add_command(label="New...")
subMenu.add_separator()
subMenu.add_command(label="Exit")

editMenu = Menu(menu)
menu.add_cascade(label="Edit", menu=editMenu)
editMenu.add_command(label="Redo")

# *** Toolbar - add commands later***

toolbar = Frame(root, bg="blue")

latestButt = Button(toolbar, text="Latest Steam Sales")
latestButt.pack(side=LEFT, padx=2, pady=2)
newButt = Button(toolbar, text="Pull New Steam Sales")
newButt.pack(side=LEFT, padx=2, pady=2)

toolbar.pack(side=TOP, fill=X)

saleFrame = Frame(root, height=1000, width=1000)
saleFrame.pack()

g = displayData()
g.display()

root.mainloop()
